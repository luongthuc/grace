jQuery(function($) {

	"use strict";
	
	var demo_grace_theme = {};
	

	// local storage for demo
	demo_grace_theme.demo_local_storage = function(){

		// header bar shown
		if (localStorage.header_bar_hidden) {
		
			$('body').addClass("topbar-hidden");
		
		}
		
		// header bar hidden
		$('body').on('click', "a[href*='?t=hidden']", function(e) {
		
			e.preventDefault();
			
			localStorage.header_bar_hidden = true;
			
			$('body').addClass("topbar-hidden");
		
		});
		
		// header bar shown
		$('body').on('click', "a[href*='?t=shown']", function(e) {
			
			e.preventDefault();
			
			localStorage.removeItem("header_bar_hidden");
			
			$('body').removeClass("topbar-hidden");
			
		});
		
		
		
		// check boxed layout
		if (localStorage.boxed_layout) {
		
			$('body').addClass("boxed");
		
		}

		// boxed layout
		$('body').on('click', "a[href*='?v=boxed']", function(e) {

			var item_class = $(this).attr('id');
			
			if(item_class !== "boxed"){
		
				e.preventDefault();
			
			}
			
			localStorage.boxed_layout = true;
			
			$('body').addClass("boxed");
			
		});
		
		// full width layout
		$('body').on('click', "a[href*='?v=full']", function(e) {
			
			e.preventDefault();
			
			localStorage.removeItem("boxed_layout");
			
			$('body').removeClass("boxed");
			
		});
		
		// full width - version page
		$('body').on('click', ".version-item-full", function() {
			localStorage.removeItem("boxed_layout");
		});
		
		// boxed width - version page
		$('body').on('click', ".version-item-boxed", function() {
			localStorage.boxed_layout = true;
		});
		
		// remove boxed on version page
		if($(".page-template-versions").length > 0){
			localStorage.removeItem("boxed_layout");
		}
		
		
		// check header style
		if (localStorage.header_style_2) {
		
			$('#header-middle').load('https://themes.lucid-themes.com/grace-wp/wp-content/themes/grace-minimal-theme/partials/demo-header-style-2.html');
			
			$('#site-header-inner > #header-navigation').hide();
		
		}
		
		// header style 1
		$('body').on('click', "a[href*='?h=style1']", function(e) {

			e.preventDefault();
			
			localStorage.removeItem("header_style_2");
			
			$('#header-middle').load('https://themes.lucid-themes.com/grace-wp/wp-content/themes/grace-minimal-theme/partials/demo-header-style-1.html');
			
			$('#site-header-inner > #header-navigation').show();
			
		});
		
		// header style 2
		$('body').on('click', "a[href*='?h=style2']", function(e) {
		
			e.preventDefault();
		
			localStorage.header_style_2 = true;
		
			$('#header-middle').load('https://themes.lucid-themes.com/grace-wp/wp-content/themes/grace-minimal-theme/partials/demo-header-style-2.html');
			
			$('#site-header-inner > #header-navigation').hide();
			
		});
		
		
	}	
		
	demo_grace_theme.demo_local_storage();
	
});