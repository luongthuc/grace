<?php

class Lottery_model extends CI_Model
{
    protected $_data;
    protected $_cate;
    protected $_data_cat;

    public function __construct()
    {
        parent::__construct();
        $this->_data = 'lottery_result';
        $this->_cate = 'lottery_category';
        $this->_data_cat = 'lottery_result_cat';
    }

    public function getResultDtoD($beg, $end)
    {
        $this->db->from($this->_data);
        $this->db->select('data_result');
        $this->db->where("displayed_time >=", date('Y-m-d', strtotime($beg)));
        $this->db->where("displayed_time <=", date('Y-m-d', strtotime($end)));
        $this->db->order_by('displayed_time', 'DESC');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }
}
