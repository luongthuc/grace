<?php

class Ahsk_model extends CI_Model
{
    function getData($select='*', $from, $where)
    {
        $this->db->select($select);
        $this->db->from($from);
        $query = $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }
    function updData($from, $data, $where)
    {
        $this->db->where($where);
        $this->db->update($from, $data);
    }




    ///////////////////////
    function register($data)
    {
        $email = $data['email'];
        $password = $data['password'];
        $this->db->select('*');
        $query = $this->db->get_where('ahsk_users', array(
            'email' => $email,
            'password' => $password
        ));
        if ($query->num_rows() == 0) {
            $this->db->insert("ahsk_users", $data);
        }
        return $query->row();
    }

    function login($data)
    {
        $email = $data['email'];
        $password = $data['password'];
        $this->db->select('*');
        $query = $this->db->get_where('ahsk_users', array(
            'email' => $email,
            'password' => $password
        ));
        // echo $this->db->last_query();
        if ($query->num_rows() == 1) {
            return $query->result_array(); // if data is true
        } else {
            return null;
        }
    }

    function update_users($data, $where)
    {
        $this->db->select('*');
        $query = $this->db->get_where('ahsk_users', ['username' => $data['username']]);
        if ($query->num_rows() == 0) {
            $this->db->where($where);
            $this->db->update('ahsk_users', $data);
            $this->db->select('*');
            $this->db->from('ahsk_users');
            $query = $this->db->where($where);
            $query = $this->db->get();
            return $query->result_array();
        } else {
            return false;
        }
    }

    function usersHeroes()
    {
        $this->db->select('*');
        $this->db->from('ahsk_uheroes');
        $query = $this->db->where(['user_id' => $_SESSION['id']]);
        $query = $this->db->get();
        return $query->result_array();
    }

    function gameHeroes($round, $game){
        $root_game = $this->db->get_where('ahsk_ugame', ['user_id' => $_SESSION['id'], 'round' => $round, 'game' => $game]);
        $root_game = $root_game->result_array();
        return $root_game;
    }

    function getMap($round, $game){
        $root_game = $this->db->get_where('ahsk_map', ['round' => $round, 'game' => $game]);
        $root_game = $root_game->result_array();
        return $root_game;
    }

    function allHeroes()
    {
        $this->db->select('*');
        $this->db->from('ahsk_heroes');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_heroes($where) {
        $this->db->select('*');
        $this->db->from('ahsk_heroes');
        $query = $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_uheroes($where) {
        $this->db->select('*');
        $this->db->from('ahsk_uheroes');
        $query = $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    function add_uheroes($data){
        $this->db->insert("ahsk_uheroes",$data);
    }

    function attack($round, $game) {
        $this->db->select('*');
        $query = $this->db->get_where('ahsk_ugame', ['user_id' => $_SESSION['id'], 'round' => $round, 'game' => $game]);
        if ($query->num_rows() == 0) {
            $root_game = $this->db->get_where('ahsk_game', ['round' => $round, 'game' => $game]);
            $root_game = $root_game->result_array();
            foreach ($root_game as $k => $data) {
                $data['user_id'] = $_SESSION['id'];
                unset($data['id']);
                $this->db->insert("ahsk_ugame",$data);
            }            
        };
    }
}
