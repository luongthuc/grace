<?php

class Category_model extends CI_Model
{
////////////////////////////
    public function loadTag() {
        $this->db->from('posts_category');
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }

    ///////////////////////////////
    public function getAllData()
    {
        return $this->db->get('posts_category')->result_array();
    }

    public function getAllDataArr()
    {
        return $this->db->get('posts_category')->result_array();
    }

    public function deleteRow($id)
    {
        $this->db->where(['id'=>$id]);
        $this->db->delete('posts_category');
    }

    public function getData($where)
    {
        return $this->db->get_where('posts_category', $where)->result_array();
    }

    public function updateData($where, $data)
    {
        $this->db->where($where);
        $this->db->update('posts_category', $data);
    }
}
