<div id="main-content">
    <!-- slideshow -->

    <!-- promo box section -->

    <!-- page content -->
    <div class="page-section">
        <div class="container">
            <div class="row">
                <!-- page content -->
                <div class="col-xlarge-8 col-medium-8 ">
                    <!-- Post list -->
                </div>
                <!-- sidebar -->
                <div class="col-xlarge-4 col-medium-4 post-sidebar right-sidebar">
                    <!-- About me -->
                    <!-- Latest posts -->
                    <!-- Follow me -->
                    <div class="sidebar-widget font-opensans-reg widget_grace_social_widget">
                        <h3 class="font-montserrat-reg">Follow me</h3>
                        <ul class="widget-social-icons">
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-pinterest"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class="fa fa-heart"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- Tag -->
                    <!-- Search -->
                    <div class="sidebar-widget font-opensans-reg widget_search">
                        <h3 class="font-montserrat-reg">Search</h3>
                        <form role="search" method="get" class="search-form" action="https://themes.lucid-themes.com/grace-wp/">
                            <label>
                                <span class="screen-reader-text">Search for:</span>
                                <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
                            </label>
                            <input type="submit" class="search-submit" value="Search" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>