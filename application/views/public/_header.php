<header id="site-header" class=" ">
    <div id="site-header-inner">
        <div id="header-top">
            <div class="container clearfix">
                <form id="header-search" role="search" method="get" action="https://themes.lucid-themes.com/grace-wp/"><button type="submit" id="submit-button"><i class="fa fa-search"></i></button><input type="text" placeholder="Type to Search..." class="font-montserrat-reg" name="s" id="s" /></form>
                <ul class="header-social">
                    <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-heart"></i></a></li>
                </ul>
            </div>
        </div>
        <div id="header-middle">
            <div class="container">
                <div class="medium-header-container clearfix"><a href="index.html" id="site-logo"><img src="public/images/grace_logo.png" alt="Site Logo"></a>
                    <div id="mobile-nav-button">
                        <div id="mobile-nav-icon"><span></span><span></span><span></span><span></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="header-navigation" class="nav-fixed">
            <div class="container">
                <nav id="header-nav">
                    <ul id="nav-ul" class="menu font-montserrat-reg clearfix">
                        <li id="menu-item-56" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-56"><a href="index.html" aria-current="page">Home</a>
                            <ul class="sub-menu">
                                <li id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-70"><a href="home/index.html">Slideshow</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-72"><a href="home/index.html">Single slide</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-71" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71"><a href="home/index.html">Narrow</a></li>
                                                <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69"><a href="home-slideshow-wide/index.html">Wide</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-158"><a href="home-slide-centered/index.html">Slide centered</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-159" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-159"><a href="home-slide-centered/index.html">Full width</a></li>
                                                <li id="menu-item-162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-162"><a href="home-slide-centered-narrow/index.html">Narrow</a></li>
                                                <li id="menu-item-165" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-165"><a href="home-slide-centered-middle/index.html">Center
                                                        narrow</a></li>
                                                <li id="menu-item-168" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-168"><a href="home-slide-centered-no-margin/index.html">No margin</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-171" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-171"><a href="home-multi-slide-2/index.html">Multi slide</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-172" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-172"><a href="home-multi-slide-2/index.html">Two slides</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-175" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-175"><a href="home-multi-slide-2-wide/index.html">Full
                                                                width</a></li>
                                                    </ul>
                                                </li>
                                                <li id="menu-item-181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-181"><a href="home-multi-slide-3/index.html">Three slides</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-180"><a href="home-multi-slide-3-wide/index.html">Full
                                                                width</a></li>
                                                    </ul>
                                                </li>
                                                <li id="menu-item-187" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-187"><a href="home-multi-slide-4/index.html">Four slides</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-197"><a href="home-banner/index.html">Banner</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-198" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-198"><a href="home-banner/index.html">Narrow</a></li>
                                        <li id="menu-item-196" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-196"><a href="home-banner-wide/index.html">Full width</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-199" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-199"><a href="home/index.html">Promo boxes</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-205" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a href="home-promo-box-2/index.html">2 Boxes</a></li>
                                        <li id="menu-item-206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-206"><a href="home-promo-box-3/index.html">3 Boxes</a></li>
                                        <li id="menu-item-200" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-200"><a href="home/index.html">4 Boxes</a></li>
                                        <li id="menu-item-317" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-317"><a href="home-promo-box-multi-row/index.html">Multi row</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-77" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-77"><a href="index.html" aria-current="page">Post list style</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-99" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-99"><a href="home-wide/index.html">Wide</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-98" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-98"><a href="home-wide-no-sidebar/index.html">No sidebar</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-106" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-106"><a href="home-small/index.html">Small</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-105" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-105"><a href="home-small-no-sidebar/index.html">No sidebar</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-112" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-112"><a href="home-grid-2/index.html">Grid</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-113" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-113"><a href="home-grid-2/index.html">2 Column</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-111" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111"><a href="home-grid-2-no-sidebar/index.html">No
                                                                sidebar</a></li>
                                                    </ul>
                                                </li>
                                                <li id="menu-item-120" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-120"><a href="home-grid-3/index.html">3 Column</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-119" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-119"><a href="home-grid-3-no-sidebar/index.html">No
                                                                sidebar</a></li>
                                                    </ul>
                                                </li>
                                                <li id="menu-item-124" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124"><a href="home-grid-4/index.html">4 Column</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-146" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-146"><a href="home-wide-then-small/index.html">Wide then small</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-145" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-145"><a href="home-wide-then-small-no-sidebar/index.html">No
                                                        sidebar</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-78" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-78"><a href="index.html" aria-current="page">Wide then grid</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-79" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-79"><a href="index.html" aria-current="page">Grid 2 column</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-82" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a href="home-wide-then-grid-2-no-sidebar/index.html">No
                                                                sidebar</a></li>
                                                    </ul>
                                                </li>
                                                <li id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-87"><a href="home-wide-then-grid-3/index.html">Grid 3 column</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-90" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90"><a href="home-wide-then-grid-3-no-sidebar/index.html">No
                                                                sidebar</a></li>
                                                    </ul>
                                                </li>
                                                <li id="menu-item-93" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-93"><a href="home-wide-then-grid-4/index.html">Grid 4 column</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-152" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-152"><a href="index.html" aria-current="page">Sidebar</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-153" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-153"><a href="index.html" aria-current="page">Right sidebar</a></li>
                                        <li id="menu-item-149" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-149"><a href="home-left-sidebar/index.html">Left sidebar</a></li>
                                        <li id="menu-item-154" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-154"><a href="home-wide-then-grid-2-no-sidebar/index.html">No sidebar</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-207" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-207"><a href="home/index.html">Examples</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-211" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-211"><a href="home-example-1/index.html">Example 1</a></li>
                                        <li id="menu-item-214" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-214"><a href="home-example-2/index.html">Example 2</a></li>
                                        <li id="menu-item-217" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-217"><a href="home-example-3/index.html">Example 3</a></li>
                                        <li id="menu-item-221" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221"><a href="home-example-4/index.html">Example 4</a></li>
                                        <li id="menu-item-227" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-227"><a href="home-example-5/index.html">Example 5</a></li>
                                        <li id="menu-item-320" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-320"><a href="home-example-6/index.html">Example 6</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li id="menu-item-36" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-36"><a href="index.html" aria-current="page">Features</a>
                            <ul class="sub-menu">
                                <li id="menu-item-37" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-37"><a href="index.html" aria-current="page">Layouts</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-38" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-38"><a href="index1ce5.html?v=full">Full</a></li>
                                        <li id="menu-item-39" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-39"><a href="index94c1.html?v=boxed">Boxed</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-40" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-40"><a href="index.html" aria-current="page">Header styles</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-41" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-41"><a href="index3c2c.html?h=style1">Style 1</a></li>
                                        <li id="menu-item-42" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-42"><a href="indexac78.html?h=style2">Style 2</a></li>
                                        <li id="menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-43"><a href="index05b5.html?t=shown">Top bar &#8211; shown</a></li>
                                        <li id="menu-item-44" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-44"><a href="index7727.html?t=hidden">Top bar &#8211; hidden</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-265" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-has-children menu-item-265"><a href="polygonal-heart/index.html">Post sidebar</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-266" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-266"><a href="polygonal-heart/index.html">Right sidebar</a></li>
                                        <li id="menu-item-308" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-308"><a href="brochure-print-design/index.html">Left sidebar</a></li>
                                        <li id="menu-item-309" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-309"><a href="thank-you-from-us/index.html">No sidebar</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-238" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-238"><a href="standard-page/index.html">Standard page</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-241" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-241"><a href="standard-page/index.html">Content type</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-242" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-242"><a href="standard-page/index.html">Text</a></li>
                                                <li id="menu-item-245" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-245"><a href="standard-page-image/index.html">Image</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-254" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-254"><a href="standard-page-image-wide/index.html">Wide</a></li>
                                                    </ul>
                                                </li>
                                                <li id="menu-item-248" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-248"><a href="standard-page-slideshow/index.html">Slideshow</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-253" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-253"><a href="standard-page-slideshow-wide/index.html">Wide</a></li>
                                                    </ul>
                                                </li>
                                                <li id="menu-item-280" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-280"><a href="standard-page-video/index.html">Video</a>
                                                    <ul class="sub-menu">
                                                        <li id="menu-item-284" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-284"><a href="standard-page-video-wide/index.html">Wide</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-237" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-237"><a href="standard-page/index.html">Sidebar</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-239" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-239"><a href="standard-page/index.html">Right sidebar</a></li>
                                                <li id="menu-item-236" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-236"><a href="standard-page-left-sidebar/index.html">Left sidebar</a></li>
                                                <li id="menu-item-235" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-235"><a href="standard-page-no-sidebar/index.html">No sidebar</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-264" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-264"><a href="test404.html">404 Page</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-297" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-297"><a href="category/branding/index.html">Categories</a>
                            <ul class="sub-menu">
                                <li id="menu-item-298" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-298"><a href="category/branding/index.html">Post list style</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-300" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-300"><a href="category/design/index.html">Wide</a></li>
                                        <li id="menu-item-301" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-301"><a href="category/project/index.html">Small</a></li>
                                        <li id="menu-item-303" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-303"><a href="category/print/index.html">Grid</a></li>
                                        <li id="menu-item-302" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-302"><a href="category/minimal/index.html">Wide then small</a></li>
                                        <li id="menu-item-299" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-299"><a href="category/branding/index.html">Wide then grid</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-304" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-304"><a href="category/branding/index.html">Sidebar</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-305" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-305"><a href="category/branding/index.html">Right sidebar</a></li>
                                        <li id="menu-item-306" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-306"><a href="category/grayscale/index.html">Left sidebar</a></li>
                                        <li id="menu-item-307" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-307"><a href="category/photography/index.html">No sidebar</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li id="menu-item-50" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50"><a href="about/index.html">About</a></li>
                        <li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49"><a href="contact/index.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>