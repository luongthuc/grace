<footer id="site-footer">
    <div id="footer-bottom">
        <div class="container">
            <div id="footer-bottom-inner" class="clearfix">
                <ul class="footer-social">
                    <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-heart"></i></a></li>
                </ul>
                <div id="scroll-top"><span class="fa fa-angle-up"></span></div>
                <p id="footer-copyright" class="font-montserrat-reg">© 2020 AcmaTvirus Editor</p>
            </div>
        </div>
    </div>
</footer>