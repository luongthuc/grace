<!DOCTYPE html>
<html lang="en-vi">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="UTF-8" />
	<!-- responsive meta tag -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="pingback" href="xmlrpc.php" />
	<title>Grace &#8211; Blog</title>
	<link rel='stylesheet' id='wp-block-library-css'
		href='<?php echo base_url('public/theme/wp-includes/css/dist/block-library/style.minf1b8.css'); ?>'
		type='text/css' media='all' />
	<link rel='stylesheet' id='contact-form-7-css'
		href='<?php echo base_url('public/theme/wp-content/plugins/contact-form-7/includes/css/styles9dff.css'); ?>'
		type='text/css' media='all' />
	<link rel='stylesheet' id='grace_main_style-css'
		href='<?php echo base_url('public/theme/wp-content/themes/grace-minimal-theme/assets/css/style.minf269.css'); ?>'
		type='text/css' media='all' />
	<script type='text/javascript'
		src='<?php echo base_url('public/theme/wp-includes/js/jquery/jquery.min9d52.js'); ?>'
		id='jquery-core-js'></script>
	<script type='text/javascript'
		src='<?php echo base_url('public/theme/wp-includes/js/jquery/jquery-migrate.mind617.js'); ?>'
		id='jquery-migrate-js'></script>
</head>
<body>
    <?php $this->load->view('public/_header'); ?>
    <?php if (!empty($main)) echo $main; ?>
    <?php $this->load->view('public/_footer'); ?>
    <div id="back-to-top"></div>
    <script type='text/javascript'
		src='<?php echo base_url('public/theme/wp-content/plugins/contact-form-7/includes/js/scripts9dff.js'); ?>'
		id='contact-form-7-js'></script>
	<script type='text/javascript'
		src='<?php echo base_url('public/theme/wp-content/themes/grace-minimal-theme/assets/js/main.min5152.js'); ?>'
		id='grace_main_js-js'></script>
	<script type='text/javascript'
		src='<?php echo base_url('public/theme/wp-content/themes/grace-minimal-theme/assets/js/demo/demo20b9.js'); ?>'
		id='grace_demo_main_js-js'></script>
	<script type='text/javascript'
		src='<?php echo base_url('public/theme/wp-includes/js/wp-embed.minf1b8.js'); ?>'
		id='wp-embed-js'></script>
	<script type='text/javascript'
		src='<?php echo base_url('public/theme/wp-content/plugins/instagram-feed/js/sbi-scripts.min61da.js'); ?>'
		id='sb_instagram_scripts-js'></script>
	<script type='text/javascript'
		src='<?php echo base_url('public/theme/wp-content/plugins/mailchimp-for-wp/assets/js/forms.mina288.js'); ?>'
		id='mc4wp-forms-api-js'></script>
</body>
</html>