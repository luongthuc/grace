<div class="sidebar-widget font-opensans-reg widget_grace_about_widget">
    <h3 class="font-montserrat-reg">About me</h3>
    <div class="page-content ">
        <p>Sed pellentesque nibh enim, quis euismod enim lacinia nec. Phasellus quam diam,
            semper in erat eu. Consectetur adipiscing elit. Sed pellentesque nibh enim, quis
            euismod enim lacinia nec.</p>
    </div>
    <ul class="widget-social-icons ">
        <li>
            <a href="#" target="_blank">
                <i class="fa fa-facebook"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                <i class="fa fa-twitter"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                <i class="fa fa-instagram"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                <i class="fa fa-pinterest"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                <i class="fa fa-heart"></i>
            </a>
        </li>
    </ul>
</div>