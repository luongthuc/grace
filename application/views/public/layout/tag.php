<div class="sidebar-widget font-opensans-reg widget_tag_cloud">
    <h3 class="font-montserrat-reg">Tags</h3>
    <div class="tagcloud">
        <a href="tag/branding/index.html" class="tag-cloud-link tag-link-15 tag-link-position-1" style="font-size: 16pt;" aria-label="branding (4 items)">branding</a>
        <a href="tag/design/index.html" class="tag-cloud-link tag-link-11 tag-link-position-2" style="font-size: 22pt;" aria-label="design (9 items)">design</a>
        <a href="tag/elegant/index.html" class="tag-cloud-link tag-link-22 tag-link-position-3" style="font-size: 8pt;" aria-label="elegant (1 item)">elegant</a>
        <a href="tag/grayscale/index.html" class="tag-cloud-link tag-link-9 tag-link-position-4" style="font-size: 14pt;" aria-label="grayscale (3 items)">grayscale</a>
        <a href="tag/heart/index.html" class="tag-cloud-link tag-link-14 tag-link-position-5" style="font-size: 8pt;" aria-label="heart (1 item)">heart</a>
        <a href="tag/minimal/index.html" class="tag-cloud-link tag-link-12 tag-link-position-6" style="font-size: 22pt;" aria-label="minimal (9 items)">minimal</a>
        <a href="tag/origami/index.html" class="tag-cloud-link tag-link-23 tag-link-position-7" style="font-size: 8pt;" aria-label="origami (1 item)">origami</a>
        <a href="tag/polygonal/index.html" class="tag-cloud-link tag-link-13 tag-link-position-8" style="font-size: 11.6pt;" aria-label="polygonal (2 items)">polygonal</a>
        <a href="tag/print/index.html" class="tag-cloud-link tag-link-17 tag-link-position-9" style="font-size: 8pt;" aria-label="print (1 item)">print</a>
        <a href="tag/project/index.html" class="tag-cloud-link tag-link-19 tag-link-position-10" style="font-size: 11.6pt;" aria-label="project (2 items)">project</a>
        <a href="tag/shopping/index.html" class="tag-cloud-link tag-link-16 tag-link-position-11" style="font-size: 8pt;" aria-label="shopping (1 item)">shopping</a>
        <a href="tag/star/index.html" class="tag-cloud-link tag-link-10 tag-link-position-12" style="font-size: 8pt;" aria-label="star (1 item)">star</a>
        <a href="tag/thanks/index.html" class="tag-cloud-link tag-link-18 tag-link-position-13" style="font-size: 8pt;" aria-label="thanks (1 item)">thanks</a>
        <a href="tag/zebra/index.html" class="tag-cloud-link tag-link-20 tag-link-position-14" style="font-size: 8pt;" aria-label="zebra (1 item)">zebra</a>
    </div>
</div>