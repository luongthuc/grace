<div class="sidebar-widget font-opensans-reg widget_grace_latest_posts_widget">
    <h3 class="font-montserrat-reg">Latest posts</h3>
    <ul class="recent_posts_list">
        <li>
            <a href="elegant-origami/index.html">
                <div class="row">
                    <div class="col-xlarge-5 col-medium-4 col-small-5">
                        <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_2.jpg" alt="" class="image" />
                    </div>
                    <div class="col-xlarge-7 col-medium-8 col-small-7 grace_latest_post_col_right">
                        <h4 class="font-montserrat-reg">Elegant Origami</h4>
                        <p class="font-opensans-reg">September 15, 2017</p>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="brochure-print-design/index.html">
                <div class="row">
                    <div class="col-xlarge-5 col-medium-4 col-small-5">
                        <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_45.jpg" alt="" class="image" />
                    </div>
                    <div class="col-xlarge-7 col-medium-8 col-small-7 grace_latest_post_col_right">
                        <h4 class="font-montserrat-reg">Brochure Print Design</h4>
                        <p class="font-opensans-reg">September 15, 2017</p>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="polygonal-heart/index.html">
                <div class="row">
                    <div class="col-xlarge-5 col-medium-4 col-small-5">
                        <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_42.jpg" alt="" class="image" />
                    </div>
                    <div class="col-xlarge-7 col-medium-8 col-small-7 grace_latest_post_col_right">
                        <h4 class="font-montserrat-reg">Polygonal Heart</h4>
                        <p class="font-opensans-reg">September 15, 2017</p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>