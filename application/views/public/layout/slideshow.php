<section class="featured-section featured-narrow">
    <div id="featured-slideshow-outer" class="carousel-outer" style="height:400px;">
        <!-- previous slide -->
        <span class="slideshow-btn previous-slide-btn fa fa-angle-left"></span>
        <!-- slideshow -->
        <div id="featured-slideshow" class="carousel " data-slide-margin="15">
            <div class="featured-slide " style="height:400px;background-image:url('public/theme/wp-content/uploads/2017/09/post_placeholder_2.jpg');">
                <div class="container">
                    <div class="featured-content-area featured-pos-center featured-align-center" style="max-width:60%;background-color:rgba(255,255,255,0.9);padding:25px;">
                        <ul class="post-categories clearfix">
                            <li class="blog-item-cat font-opensans-reg">
                                <a href="category/project/index.html"> Project </a>
                            </li>
                        </ul>
                        <h2 class="font-montserrat-reg" style="">
                            <a href="elegant-origami/index.html" style="">
                                Elegant Origami
                            </a>
                        </h2>
                        <p class="font-opensans-reg">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Sed pellentesque nibh enim, quis euismod enim lacinia nec. Phasellus quam diam,
                            semper in erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
                    </div>
                </div>
            </div>
            <div class="featured-slide " style="height:400px;background-image:url('public/theme/wp-content/uploads/2017/09/post_placeholder_45.jpg');">
                <div class="container">
                    <div class="featured-content-area featured-pos-center featured-align-center" style="max-width:60%;background-color:rgba(255,255,255,0.9);padding:25px;">
                        <ul class="post-categories clearfix">
                            <li class="blog-item-cat font-opensans-reg">
                                <a href="category/branding/index.html"> Branding </a>
                            </li>
                            <li class="blog-item-cat font-opensans-reg">
                                <a href="category/print/index.html"> Print </a>
                            </li>
                            <li class="blog-item-cat font-opensans-reg">
                                <a href="category/project/index.html"> Project </a>
                            </li>
                        </ul>
                        <h2 class="font-montserrat-reg" style="">
                            <a href="brochure-print-design/index.html" style="">

                                Brochure Print Design
                            </a>
                        </h2>
                        <p class="font-opensans-reg">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Sed pellentesque nibh enim, quis euismod enim lacinia nec. Phasellus quam diam,
                            semper in erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
                    </div>
                </div>
            </div>
            <div class="featured-slide " style="height:400px;background-image:url('public/theme/wp-content/uploads/2017/09/post_placeholder_42.jpg');">
                <div class="container">
                    <div class="featured-content-area featured-pos-center featured-align-center" style="max-width:60%;background-color:rgba(255,255,255,0.9);padding:25px;">
                        <ul class="post-categories clearfix">
                            <li class="blog-item-cat font-opensans-reg">
                                <a href="category/design/index.html"> Design </a>
                            </li>
                            <li class="blog-item-cat font-opensans-reg">
                                <a href="category/minimal/index.html"> Minimal </a>
                            </li>
                        </ul>
                        <h2 class="font-montserrat-reg" style="">
                            <a href="polygonal-heart/index.html" style="">

                                Polygonal Heart
                            </a>
                        </h2>
                        <p class="font-opensans-reg">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Sed pellentesque nibh enim, quis euismod enim lacinia nec. Phasellus quam diam,
                            semper in erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
                    </div>
                </div>
            </div>
            <div class="featured-slide " style="height:400px;background-image:url('public/theme/wp-content/uploads/2017/09/post_placeholder_46.jpg');">
                <div class="container">
                    <div class="featured-content-area featured-pos-center featured-align-center" style="max-width:60%;background-color:rgba(255,255,255,0.9);padding:25px;">
                        <ul class="post-categories clearfix">
                            <li class="blog-item-cat font-opensans-reg">
                                <a href="category/branding/index.html"> Branding </a>
                            </li>
                            <li class="blog-item-cat font-opensans-reg">
                                <a href="category/print/index.html"> Print </a>
                            </li>
                        </ul>
                        <h2 class="font-montserrat-reg" style="">
                            <a href="business-rebrand/index.html" style="">

                                Business Rebrand
                            </a>
                        </h2>
                        <p class="font-opensans-reg">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Sed pellentesque nibh enim, quis euismod enim lacinia nec. Phasellus quam diam,
                            semper in erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- next slide -->
        <span class="slideshow-btn next-slide-btn fa fa-angle-right"></span>
    </div>
</section>