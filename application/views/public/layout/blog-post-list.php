<ul class="blog-post-list post-list row ">
    <!-- Wide post item -->
    <li class="col-xlarge-12">
        <div id="post-294" class="post-list-item wide-post-list-item blog-list-item post-item-left post-294 post type-post status-publish format-standard has-post-thumbnail hentry category-project tag-elegant tag-origami tag-project">
            <a href="elegant-origami/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_2.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/project/index.html">Project</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="elegant-origami/index.html">
                    Elegant Origami </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum mi. Sed interdum mattis
                    risus, sit amet eleifend ligula luctus ut. Sed ullamcorper lorem aliquam,
                    tincidunt lorem et, ultrices est. Suspendisse eleifend dui odio, id&hellip;
                </p>
            </div>
            <a href="elegant-origami/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <li class="clearfix"></li>
    <!-- Wide post item -->
    <li class="col-xlarge-6">
        <div id="post-29" class="post-list-item wide-post-list-item blog-list-item post-item-left post-29 post type-post status-publish format-standard has-post-thumbnail hentry category-branding category-print category-project tag-branding tag-design tag-minimal tag-print">
            <a href="brochure-print-design/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_45.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/branding/index.html">Branding</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/print/index.html">Print</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/project/index.html">Project</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="brochure-print-design/index.html">
                    Brochure Print Design </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
            </div>
            <a href="brochure-print-design/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <!-- Wide post item -->
    <li class="col-xlarge-6">
        <div id="post-19" class="post-list-item wide-post-list-item blog-list-item post-item-left post-19 post type-post status-publish format-standard has-post-thumbnail hentry category-design category-minimal tag-design tag-heart tag-minimal tag-polygonal">
            <a href="polygonal-heart/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_42.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/design/index.html">Design</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/minimal/index.html">Minimal</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="polygonal-heart/index.html">
                    Polygonal Heart </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
            </div>
            <a href="polygonal-heart/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <li class="clearfix"></li>
    <!-- Wide post item -->
    <li class="col-xlarge-6">
        <div id="post-17" class="post-list-item wide-post-list-item blog-list-item post-item-left post-17 post type-post status-publish format-standard has-post-thumbnail hentry category-branding category-print tag-branding tag-design tag-grayscale tag-minimal">
            <a href="business-rebrand/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_46.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/branding/index.html">Branding</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/print/index.html">Print</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="business-rebrand/index.html">
                    Business Rebrand </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
            </div>
            <a href="business-rebrand/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <!-- Wide post item -->
    <li class="col-xlarge-6">
        <div id="post-23" class="post-list-item wide-post-list-item blog-list-item post-item-left post-23 post type-post status-publish format-standard has-post-thumbnail hentry category-design category-project tag-design tag-grayscale tag-minimal tag-zebra">
            <a href="black-and-white-zebra/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_43.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/design/index.html">Design</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/project/index.html">Project</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="black-and-white-zebra/index.html">
                    Black and White Zebra </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
            </div>
            <a href="black-and-white-zebra/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <li class="clearfix"></li>
    <!-- Wide post item -->
    <li class="col-xlarge-6">
        <div id="post-6" class="post-list-item wide-post-list-item blog-list-item post-item-left post-6 post type-post status-publish format-standard has-post-thumbnail hentry category-branding category-photography category-print tag-branding tag-design tag-minimal tag-shopping">
            <a href="shopping-bag-redesign/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_44.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/branding/index.html">Branding</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/photography/index.html">Photography</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/print/index.html">Print</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="shopping-bag-redesign/index.html">
                    Shopping bag redesign </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
            </div>
            <a href="shopping-bag-redesign/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <!-- Wide post item -->
    <li class="col-xlarge-6">
        <div id="post-21" class="post-list-item wide-post-list-item blog-list-item post-item-left post-21 post type-post status-publish format-standard has-post-thumbnail hentry category-design category-minimal tag-design tag-minimal tag-polygonal">
            <a href="graphic-design-work/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_40.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/design/index.html">Design</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/minimal/index.html">Minimal</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="graphic-design-work/index.html">
                    Graphic design work </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
            </div>
            <a href="graphic-design-work/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <li class="clearfix"></li>
    <!-- Wide post item -->
    <li class="col-xlarge-6">
        <div id="post-287" class="post-list-item wide-post-list-item blog-list-item post-item-left post-287 post type-post status-publish format-standard has-post-thumbnail hentry category-design category-grayscale tag-design tag-grayscale tag-minimal tag-star">
            <a href="grayscale-star/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_1.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/design/index.html">Design</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/grayscale/index.html">Grayscale</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="grayscale-star/index.html">
                    Grayscale Star </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
            </div>
            <a href="grayscale-star/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <!-- Wide post item -->
    <li class="col-xlarge-6">
        <div id="post-25" class="post-list-item wide-post-list-item blog-list-item post-item-left post-25 post type-post status-publish format-standard has-post-thumbnail hentry category-branding category-project tag-branding tag-design tag-minimal tag-project">
            <a href="client-branding-project/index.html">
                <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_41.jpg" alt="" class="image">
            </a>
            <!-- blog item categories -->
            <ul class="post-categories clearfix">
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/branding/index.html">Branding</a>
                </li>
                <li class="blog-item-cat font-opensans-reg">
                    <a href="category/project/index.html">Project</a>
                </li>
            </ul>
            <h3 class="font-montserrat-reg">
                <a href="client-branding-project/index.html">
                    Client branding project </a>
            </h3>
            <div class="post-list-item-meta font-opensans-reg clearfix">
                <span>September 15, 2017</span>
                <span>Lucid Themes</span>
            </div>
            <div class="page-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque
                    nibh enim, quis euismod enim lacinia nec. Phasellus quam diam, semper in
                    erat eu, efficitur molestie purus. Sed a elementum&hellip;</p>
            </div>
            <a href="client-branding-project/index.html" class="primary-button font-montserrat-reg hov-bk">Read more</a>
        </div>
    </li>
    <li class="clearfix"></li>
</ul>
<!-- Blog posts navigation -->
<div class="post-list-pagination">
    <section class="post-navigation">
        <div id="post-nav-main" class="clearfix">
            <span aria-current="page" class="page-numbers current">1</span>
            <a class="page-numbers" href="page/2/index.html">2</a>
            <a class="next page-numbers" href="page/2/index.html">
                <span id="post-nav-next" class="post-nav-item font-montserrat-reg">Older
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </section>
</div>