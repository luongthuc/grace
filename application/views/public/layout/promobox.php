<section class="promo-box-section">
    <div class="container">
        <ul class="row">
            <li class="col-xlarge-3 col-medium-6">
                <a href="category/design/index.html" class="promo-box-item" target="_self">
                    <div class="promo-box-hover"></div>
                    <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_40.jpg" alt="" class="image" />
                    <div class="promo-item-inside font-opensans-reg promo-inside-center ">
                        <h3 style="background-color:rgba(255,255,255,0.9);max-width:80%;color:#111111" class="promo-item-padding" style="color:#111111">Design</h3>
                    </div>
                </a>
            </li>
            <li class="col-xlarge-3 col-medium-6">
                <a href="category/branding/index.html" class="promo-box-item" target="_self">
                    <div class="promo-box-hover"></div>
                    <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_41.jpg" alt="" class="image" />
                    <div class="promo-item-inside font-opensans-reg promo-inside-center ">
                        <h3 style="background-color:rgba(255,255,255,0.9);max-width:80%;color:#111111" class="promo-item-padding" style="color:#111111">Branding</h3>
                    </div>
                </a>
            </li>
            <li class="col-xlarge-3 col-medium-6">
                <a href="category/minimal/index.html" class="promo-box-item" target="_self">
                    <div class="promo-box-hover"></div>
                    <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_42.jpg" alt="" class="image" />
                    <div class="promo-item-inside font-opensans-reg promo-inside-center ">
                        <h3 style="background-color:rgba(255,255,255,0.9);max-width:80%;color:#111111" class="promo-item-padding" style="color:#111111">Minimal</h3>
                    </div>
                </a>
            </li>
            <li class="col-xlarge-3 col-medium-6">
                <a href="category/print/index.html" class="promo-box-item" target="_self">
                    <div class="promo-box-hover"></div>
                    <img src="public/theme/wp-content/uploads/2017/09/post_placeholder_46.jpg" alt="" class="image" />
                    <div class="promo-item-inside font-opensans-reg promo-inside-center ">
                        <h3 style="background-color:rgba(255,255,255,0.9);max-width:80%;color:#111111" class="promo-item-padding" style="color:#111111">Print</h3>
                    </div>
                </a>
            </li>
            <li class="clearfix"></li>
        </ul>
    </div>
</section>
<section class="newsletter-section">
    <div class="container">
        <div class="page-newsletter clearfix">
            <script>
                (function() {
                    window.mc4wp = window.mc4wp || {
                        listeners: [],
                        forms: {
                            on: function(evt, cb) {
                                window.mc4wp.listeners.push({
                                    event: evt,
                                    callback: cb
                                });
                            }
                        }
                    }
                })();
            </script>
            <!-- Mailchimp for WordPress v4.8.1 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
            <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-57" method="post" data-id="57" data-name="Newsletter">
                <div class="mc4wp-form-fields">
                    <div class="page-newsletter-text">
                        <p class="first hidden-sidebar">Join our newsletter</p>
                        <p class="second">Stay in touch by joining our weekly newsletter</p>
                    </div>
                    <div class="page-newsletter-form">
                        <input type="email" name="EMAIL" placeholder="Your email address" required />
                        <input type="submit" value="Subscribe" class="primary-button hov-bk" />
                    </div>
                </div>
                <label style="display: none !important;">Leave this field empty if you're human:
                    <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" />
                </label>
                <input type="hidden" name="_mc4wp_timestamp" value="1611916996" />
                <input type="hidden" name="_mc4wp_form_id" value="57" />
                <input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1" />
                <div class="mc4wp-response"></div>
            </form>
            <!-- / Mailchimp for WordPress Plugin -->
        </div>
    </div>
</section>