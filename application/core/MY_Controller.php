<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $template_main;
    public function __construct()
    {
        parent::__construct();
        // config path
        $template_main = 'public/layout';
    }
    function set_sess($data) {
        $this->session->set_userdata($data);
    }
    
    function remove_sess($data) {
        $this->session->unset_userdata($data);
    }
}
