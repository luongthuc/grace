<?php

function menumain()
{
    $ci = &get_instance();
    /// SETTING //
    $db_table = 'arena_menus';
    ///////////////
    $ci->db->select("*");
    $ci->db->where(["parent" => "0"]);
    $query = $ci->db->get($db_table);
    $data_menu = $query->result_array();
    $html = '';
    $htmlChild = '';
    for ($i = 0; $i < count($data_menu); $i++) {
        $htmlChild = '';
        if ($data_menu[$i]['child'] == 1) {
            $ci->db->select("*");
            $ci->db->where(["parent" => $data_menu[$i]['id']]);
            $queryChild = $ci->db->get($db_table);
            $data_menuChild = $queryChild->result_array();
            $htmlChild .= '<ul class="sub-menu">';
            for ($i2 = 0; $i2 < count($data_menuChild); $i2++) {
                $htmlChild .= '<li class=""><a href="' . base_url('/' . $data_menuChild[$i2]['url']) . '">' . $data_menuChild[$i2]['name'] . '</a></li>';
            }
            $htmlChild .= '</ul>';
        }

        if (strlen($htmlChild) > 0) {
            $html .= '<li class="has-children"><a href="' . base_url('/' . $data_menu[$i]['url']) . '">' . $data_menu[$i]['name'] . '</a>' . $htmlChild . '</li>';
        } else {
            $html .= '<li><a href="' . base_url('/' . $data_menu[$i]['url']) . '">' . $data_menu[$i]['name'] . '</a>' . $htmlChild . '</li>';
        }
    }
    $html .= '</ul></nav>';
    return $html;
}

function pagination($db, $pag=1)
{
    $ci = &get_instance();
    /// SETTING //
    // load db and model
    $ci->load->model('Pagination_model');
    $paginationModel = $ci->Pagination_model;
    $total = $paginationModel->get_total($db);
    // SETTING HEAD PAGINATION
    $params = '';
    $nshow_pag = 3;
    $params .= '<ul class="pagination">';
    if($pag > 0) $params .= '<li class="showpageNum"><a href="#" data-page="'.($pag-1).'">Pre</a></li>';
    if ($pag > 0) $params .= '<li class="showpagePoint">'.($pag).'</li>';
    for($i=1; $i< $nshow_pag; $i++) {
        if (($pag+$i) < (ceil($total/10))) $params .= '<li class="showpageNum"><a href="#" data-page="'.($pag+$i).'">'.($pag+$i).'</a></li>';
    }
    if($pag < $total) $params .= '<li class="showpageNum"><a href="#" data-page="'.ceil($total/10).'">'.ceil($total/10).'</a></li>';
    if($pag < $total) $params .= '<li class="showpageNum"><a href="#" data-page="'.($pag+1).'">Next</a></li>'; 
    $params .= '</ul>';
    return $params;
}
